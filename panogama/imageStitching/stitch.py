import imutils
from imutils import paths
import cv2
import numpy as np

from . import processing

# Take picture from folder like: Hill1 & Hill2, scene1 & scene2, my1 & my2, taj1 & taj2, lotus1 & lotus2, beach1 & beach2, room1 & room2
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

path = dir_path + "\\static\\upload\\"

# load image from folder
def loadImages(path, resize):
    '''Load Images from @path to list, if @resize=true, image will be resized'''
    image_path = list(paths.list_images(path))
    list_image = []
    for i, j in enumerate(image_path):
        image = cv2.imread(j)
        if resize == 1:
            image = imutils.resize(image, width=600)
            image = imutils.resize(image, height=600)
            #image = cv2.resize(image, (400, 400))
        list_image.append(image)
    return (list_image)

#Improve Image Result stitch.
def improve(stitched):
        # create a 10 pixel border surrounding the stitched image
		stitched=cv2.copyMakeBorder(stitched, 10, 10, 10, 10,cv2.BORDER_CONSTANT, (0, 0, 0))
        # convert the stitched image to grayscale and threshold it
        # such that all pixels greater than zero are set to 255
        # (foreground) while all others remain 0 (background)
		gray=cv2.cvtColor(stitched,cv2.COLOR_BGR2GRAY)
		thresh=cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)[1]
        # find all external contours in the threshold image then find
        # the *largest* contour which will be the contour/outline of
        # the stitched image
		cnts=cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
		cnts=imutils.grab_contours(cnts)
		c = max(cnts, key=cv2.contourArea)
        # allocate memory for the mask which will contain the
        # rectangular bounding box of the stitched image region
		mask = np.zeros(thresh.shape, dtype="uint8")
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(mask, (x, y), (x + w, y + h), 255, -1)
        # create two copies of the mask: one to serve as our actual
        # minimum rectangular region and another to serve as a counter
        # for how many pixels need to be removed to form the minimum
        # rectangular region
		minRect = mask.copy()
		sub = mask.copy()
        # keep looping until there are no non-zero pixels left in the
        # subtracted image
		while cv2.countNonZero(sub) > 0:
            # erode the minimum rectangular mask and then subtract
            # the thresholded image from the minimum rectangular mask
            # so we can count if there are any non-zero pixels left
			minRect = cv2.erode(minRect, None)
			sub = cv2.subtract(minRect, thresh)
        # find contours in the minimum rectangular mask and then
        # extract the bounding box (x, y)-coordinates
		cnts = cv2.findContours(minRect.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		c = max(cnts, key=cv2.contourArea)
		(x, y, w, h) = cv2.boundingRect(c)
        # use the bounding box coordinates to extract the our final
        # stitched image
		stitched = stitched[y:y + h, x:x + w]
		return stitched

#Panorama
class Run:
    def run(self):
        images = loadImages(path, 1)
        no_of_images = len(images)
        panaroma = processing.Panaroma()
        if no_of_images == 2:
            (result, matched_points) = panaroma.image_stitch(
                [images[0], images[1]], match_status=True)
        else:
            (result, matched_points) = panaroma.image_stitch(
                [images[no_of_images-2], images[no_of_images-1]], match_status=True)
            for i in range(no_of_images - 2):
                (result, matched_points) = panaroma.image_stitch(
                    [images[no_of_images-i-3], result], match_status=True)
    # def run(self):
    #     list_images = loadImages(path,1)
    #     panaroma = processing.Panaroma()
    #     if(len(list_images)==2):
    #         (result, matched_points) = panaroma.image_stitch(
    #             [list_images[0], list_images[1]], match_status=True)
    #     elif (len(list_images)>2):
    #         n=int(len(list_images)/2+0.5)
    #         left=list_images[:n]
    #         right=list_images[n-1:]
    #         right.reverse()   
    #         while len(left)>1:
    #             dst_img=left.pop()
    #             src_img=left.pop()
    #             (left_pano, matched_points)=panaroma.image_stitch([src_img,dst_img], match_status=True)
    #             left_pano=left_pano.astype('uint8')
    #             left.append(left_pano)

    #         while len(right)>1:
    #             dst_img=right.pop()
    #             src_img=right.pop()
    #             (right_pano, matched_points)=panaroma.image_stitch([src_img,dst_img],match_status=True)
    #             right_pano=right_pano.astype('uint8')
    #             right.append(right_pano)

    #         #if width_right_pano > width_left_pano, Select right_pano as destination. Otherwise is left_pano
    #         if(right_pano.shape[1]>=left_pano.shape[1]):
    #             (result, matched_points)=panaroma.image_stitch([left_pano,right_pano],match_status=True)
    #         else:
    #             (result, matched_points)=panaroma.image_stitch([right_pano,left_pano],match_status= True)
    #     else:
    #         raise Exception("Select at least 2 photos to create panorama ")
        #return fullpano


        result = improve(result)

        # to write the images is result of panorama process
        cv2.imwrite(dir_path + "\\static\\result\\matching.jpg", matched_points)
        cv2.imwrite(dir_path + "\\static\\result\\stitching.jpg", result)
        