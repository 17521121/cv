import shutil
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import os
from . import stitch
# # Create your views here.



def index(request):
    template = 'index.html'
    return render(request, template)


def upload(request):
    if request.method == 'POST':
        try:
            shutil.rmtree('imageStitching/static/upload')
        except:
            pass
        try:
            os.mkdir("imageStitching/static/upload")
        except:
            pass
        try:
            os.mkdir("imageStitching/static/result")
        except:
            pass  
        fs = FileSystemStorage()
        myfiles = request.FILES.getlist('folder')

        for f in myfiles:
            file = fs.save("imageStitching/static/upload/" + f._get_name(), f)

       
        run = stitch.Run()
        run.run()

        return HttpResponse("ok")
